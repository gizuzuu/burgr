#include "math.h"

int pow_a(int base, int exp)
{
  int result = base;

  for (int i = 0; i < exp - 1; i++)
  {
    result *= base;
  }

  return result;
}

double sqrt_a(double n)
{
  // Max and min are used to take into account numbers less than 1
  double lo = 1, hi = n, mid;

  // Update the bounds to be off the target by a factor of 10
  while (100 * lo * lo < n)
    lo *= 10;
  while (0.01 * hi * hi > n)
    hi *= 0.1;

  for (int i = 0; i < 100; i++)
  {
    mid = (lo + hi) / 2;
    if (mid * mid == n)
      return mid;
    if (mid * mid > n)
      hi = mid;
    else
      lo = mid;
  }

  return mid;
}

int ipow(int base, int exp)
{
    int result = 1;
    for (;;)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        if (!exp)
            break;
        base *= base;
    }

    return result;
}