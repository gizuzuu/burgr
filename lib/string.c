#include "string.h"

int strcmp_a(char *str1, char *str2)
{
  while (*str1 && *str2 && *str1 == *str2)
  {
    str1++;
    str2++;
  }

  return *str1 - *str2;
}

char *strcpy_a(char *dest, const char *src) // идем до конца строки (src) и переносит в дест
{
  char *r = dest;

  while (*src)
  {
    *(dest++) = *(src++);
  }
  *dest = '\0';

  return r;
}