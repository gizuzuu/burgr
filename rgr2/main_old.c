#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "../lib/string.h"
#include "../lib/math.h"

const int ARRAY_LENGTH = 10000;     // Это N
const int ARRAY_STRING_LENGTH = 20; // Это char[20]

const double GOLDEN_RATIO = 1.61803398875;                // Золотое сечение
const double CLOCKS_PER_MILLISEC = CLOCKS_PER_SEC / 1000; // Для вывода времени в миллисекундах, а не секундах

/*
  Генерация (заполнение) псевдослучайной строки str из size символов
*/
void generateRandomString(char *str, int size)
{
  char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (int i = 0; i < size - 1; i++)
  {
    // Выборка случайного индекса из charset
    int key = rand() % (int)(sizeof(charset) - 1);
    // Заполнение строки str случайным символом
    str[i] = charset[key];
  }

  // Установка метки конца строки
  str[size - 1] = '\0';
}

/*
  Заполнение массива строк arr длинной size случайными строками
*/
void fillArrayWithStrings(char **arr, int size)
{
  for (int i = 0; i < size; i++)
  {
    // Выделение памяти для строки
    arr[i] = (char *)malloc(ARRAY_STRING_LENGTH);
    // Генерация случайной строки
    generateRandomString(arr[i], ARRAY_STRING_LENGTH);
  }
}

/*
  Заполнение массива arr случайными выборками из source.
  size_arr - длинна массива arr, size_source - длинна масива source

  use_fake_string - 1, если надо генерировать элементы, не состоящие в массиве
    0, если надо генерировать элементы, состоящие в массиве
*/
void fillArrayWithRandomSelectedStrings(char **arr, char **source, int size_arr, int size_source, int use_fake_string)
{
  for (int i = 0; i < size_arr; i++)
  {
    // Выделение памяти для строки
    arr[i] = (char *)malloc(ARRAY_STRING_LENGTH);

    if (use_fake_string)
      strcpy_a(arr[i], "123af2253d\0"); // Вставка нереальной строки
    else
      strcpy_a(arr[i], source[rand() % size_source]); // Вставка одной из строк массива source
  }
}

/*
  Очистка массива строк
*/
void purgeArray(char **arr, int size)
{
  for (int i = 0; i < size; i++)
  {
    free(arr[i]);
  }

  free(arr);
}

/*
  Линейный поиск. Идём до конца массива с начала и ищем совпадения, после чего возвращаем
  индекс совпадения или -1, если ничего не было найдено
*/
int linearSearch(char **arr, char *target, int size)
{
  for (int i = 0; i < size; i++)
  {
    if (strcmp_a(arr[i], target) == 0)
      return i;
  }

  return -1;
}

/*
  Поиск золотым сечением.
  Принимает отсортированный массив строк arr размером size и целевую строку target, которую нужно найти.

  Затем инициализируются переменные start, end, mid1 и mid2.
  start и end определяют диапазон поиска в массиве
  mid1 и mid2 - это две точки в этом диапазоне, разделенные золотым сечением (1.618).

  Цикл while продолжается, пока целевая строка не найдена и пока start меньше end.
  В каждой итерации цикла диапазон поиска сужается, исходя из того,
  где целевая строка находится относительно mid1 и mid2.

  Если целевая строка найдена в mid1 или mid2, функция возвращает соответствующий индекс.
  Если целевая строка не найдена после завершения цикла, функция возвращает -1.
*/
// int goldenSectionSearch(char *arr[], int size, char *target)
// {
//   int start = 0;
//   int end = size - 1;
//   int mid1 = end / GOLDEN_RATIO;
//   int mid2 = start + (end - mid1);

//   while (strcmp_a(target, arr[mid1]) != 0 && strcmp_a(target, arr[mid2]) != 0 && start < end)
//   {
//     if (strcmp_a(target, arr[mid1]) < 0)
//     {
//       end = mid2 - 1;
//     }
//     else if (strcmp_a(target, arr[mid2]) < 0)
//     {
//       start = mid1 + 1;
//       end = mid2 - 1;
//     }
//     else
//     {
//       start = mid2 + 1;
//     }

//     mid1 = end - (end - start) / GOLDEN_RATIO;
//     mid2 = start + (end - mid1);
//   }

//   if (strcmp_a(target, arr[mid1]) == 0)
//   {
//     return mid1;
//   }
//   else if (strcmp_a(target, arr[mid2]) == 0)
//   {
//     return mid2;
//   }

//   return -1;
// }

int min(int x, int y) { return (x <= y) ? x : y; };

int goldenSectionSearch(char *arr[], int size, char *target)
{
  int low = 0;
  int high = size - 1;
  int mid = high / GOLDEN_RATIO;

  // int count = 0;

  // while (mid != low || mid != high)
  // {
  //   //if (count == 20) break;

  //   int res = strcmp_a(arr[mid], target);
  //   printf("L: %d, H: %d, M: %d, R: %d\n", low, high, mid, res);
  //   printf("%s %s\n", arr[mid], target);

  //   if (res == 0)
  //     return mid;
  //   else if (res > 0)
  //   {
  //     high = mid;
  //     mid = low + (high - low) / GOLDEN_RATIO;
  //   }
  //   else
  //   {
  //     low = mid;
  //     mid = low + (high - low) / GOLDEN_RATIO;
  //   }

  //   count++;
  // }

  while (strcmp_a(target, arr[mid]) != 0 && low < high && mid != 1)
  {
    int res = strcmp_a(target, arr[mid]);

    if (res < 0)
    {
      high = mid - 1;
    }
    else
    {
      low = mid + 1;
    }

    if (high - low == 1)
      mid = low + 1;
    else
      mid = low + (high - low) / GOLDEN_RATIO;

    // printf("L: %d, H: %d, M: %d, R: %d\n", low, high, mid, res);
    // printf("%s %s\n", arr[mid], target);
  }

  if (strcmp_a(arr[mid], target) == 0)
    return mid;

  return -1;
}

/*
  Сортировка Шелла.
  Принимает массив строк arr длинной size

  В начале функции вычисляется максимальное значение gap,
  которое будет использоваться в алгоритме. Это делается с помощью цикла for,
  который увеличивает gap до тех пор, пока оно не станет больше size.
  Значение gap вычисляется как 2^p - 1, где p - это текущая итерация цикла.

  Затем алгоритм выполняет серию “прогонов” через массив,
  начиная с максимального значения gap и уменьшая его на каждой итерации.
  На каждом прогоне элементы, разделенные текущим значением gap, сравниваются и,
  при необходимости, меняются местами.

  Внутренний цикл for проходит через массив, начиная с текущего значения gap и до конца массива.
  На каждой итерации выбирается элемент arr[i],
  и затем выполняется серия сравнений и обменов с элементами,
  находящимися на расстоянии gap слева от него.

  Если элемент arr[j - gap] больше текущего элемента temp, они меняются местами.
  Это продолжается, пока не будет найден элемент, который меньше temp,
  или пока не будет достигнут начало массива.
*/
void shellSort(char **arr, int size)
{
  // Вычисление максимального значения p, чтобы идти с конца, а не начала
  int max_p = 1;
  for (int p = 1, gap = 1; gap < size; gap = pow_a(2, p) - 1, p++)
    max_p = p;
  max_p -= 1;

  for (int p = max_p, gap = pow_a(2, max_p) - 1; p >= 0; gap = pow_a(2, p) - 1, p--)
  {
    for (int i = gap; i < size; i++)
    {
      char *temp = arr[i];
      int j;
      for (j = i; j >= gap && strcmp_a(arr[j - gap], temp) > 0; j -= gap) // сравнивает две строки лексикографически.
        arr[j] = arr[j - gap];
      arr[j] = temp;
    }
  }
}

struct SearchResults
{
  int array_length;
  int find_array_length;
  double linear_time;
  double sort_time;
  double gold_time;
  int use_fake_string;
};

struct SearchResults runSearch(char **arr, int size, int m, int use_fake_string)
{
  // Генерация массива случайных строк
  printf("Генерация массива случайных слов...\n");
  fillArrayWithStrings(arr, ARRAY_LENGTH);

  // Выборка из массива случайных строк
  printf("Выборка %d случайных строк\n", m);
  char **find_arr = calloc(m, sizeof(char *));
  fillArrayWithRandomSelectedStrings(find_arr, arr, m, size, use_fake_string);

  // Запоминание общего времени выполнения линейного поиска (в тактах)
  double lin_sum = 0;
  printf("Использование линейного поиска:\n");
  for (int i = 0; i < m; i++)
  {
    printf("%d итерация | ", i + 1);

    // Линейный поиск с расчётом времени
    clock_t lin_start = clock();
    int result = linearSearch(arr, find_arr[i], size);
    clock_t lin_end = clock();

    if (result == -1)
      printf("Строка не найдена | ");
    else
      printf("Строка найдена | ");

    printf("Время: %.6f мс.\n", (double)(lin_end - lin_start) / CLOCKS_PER_MILLISEC);

    // Добавление времени выполнения одной задачи в общее время
    lin_sum += lin_end - lin_start;
  }

  // Сортировка массива строк методом Шелла с расчётом времени
  printf("Сортировка массива методом Шелла и последовательностью Хаббарда\n");
  clock_t srt_start = clock();
  shellSort(arr, ARRAY_LENGTH);
  clock_t srt_end = clock();

  // Запоминание общего времени выполнения линейного поиска (в тактах)
  double gld_sum = 0;
  printf("Использование поиска золотым сечением:\n");
  for (int i = 0; i < m; i++)
  {
    printf("%d итерация | ", i + 1);

    // Поиск золотым сечением с расчётом времени
    clock_t gld_start = clock();
    int result = goldenSectionSearch(arr, size, find_arr[i]);
    clock_t gld_end = clock();

    if (result == -1)
      printf("Строка не найдена | ");
    else
      printf("Строка найдена | ");

    printf("Время: %.6f мс.\n", (double)(gld_end - gld_start) / CLOCKS_PER_MILLISEC);

    // Добавление времени выполнения одной задачи в общее время
    gld_sum += gld_end - gld_start;
  }

  purgeArray(find_arr, m);

  struct SearchResults results = {
      .array_length = size,
      .find_array_length = m,
      .linear_time = lin_sum,
      .sort_time = srt_end - srt_start,
      .gold_time = gld_sum,
      .use_fake_string = use_fake_string};

  return results;
}

struct SearchResults detectMiddle(char **arr, int size, int use_fake_string)
{
  int mid_m = 10;

  struct SearchResults results = runSearch(arr, ARRAY_LENGTH, mid_m, use_fake_string);
  purgeArray(arr, ARRAY_LENGTH);

  int l = 0;
  int r = 10000;
  while (r - l > 1)
  {
    int mid = (r + l) / 2;
    if (mid_m < size && (results.gold_time + results.sort_time) <= results.linear_time)
      l = mid;
    else
      r = mid;
  }
  

  // while (mid_m < size && (results.gold_time + results.sort_time) > results.linear_time)
  // {
  //   mid_m += 10;

  //   arr = calloc(ARRAY_LENGTH, sizeof(char *));
  //   results = runSearch(arr, ARRAY_LENGTH, mid_m, use_fake_string);
  //   purgeArray(arr, ARRAY_LENGTH);
  // }

  results.find_array_length = r;

  return results;
}

int main()
{
  // Выделение памяти для массива строк
  char **arr = calloc(ARRAY_LENGTH, sizeof(char *));

  // Ввод параметров
  int m;
  printf("Введите m (0 для автоопределения): ");
  scanf("%d", &m);

  if (m > ARRAY_LENGTH || m < 0)
  {
    printf("Число m должно быть от 0 до %d! Завершение программы\n", ARRAY_LENGTH);
    exit(0);
  }

  int use_fake_string;
  printf("Использовать несуществующие строки (1 - да, 0 - нет)? ");
  scanf("%d", &use_fake_string);

  if (m == 0)
  {
    struct SearchResults results = detectMiddle(arr, ARRAY_LENGTH, use_fake_string);

    printf("\n=========================== НАЙДЕНА СЕРЕДИНА ======================\n");
    printf("📟 Размер массива строк (N): %d | Нужно было найти (m): %d\n", ARRAY_LENGTH, results.find_array_length);
    printf("🔎 Общее время выполнения линейного поиска: %.6f мс.\n", results.linear_time / CLOCKS_PER_MILLISEC);
    printf("🔀 Время сортировки Шелла: %.6f мс.\n", results.sort_time / CLOCKS_PER_MILLISEC);
    printf("🟨 Общее время выполнения поиска золотым сечением: %.6f мс.\n", results.gold_time / CLOCKS_PER_MILLISEC);
  }
  else
  {
    struct SearchResults results = runSearch(arr, ARRAY_LENGTH, m, use_fake_string);

    printf("\n===================================================================\n");
    printf("📟 Размер массива строк (N): %d | Нужно было найти (m): %d\n", ARRAY_LENGTH, results.find_array_length);
    printf("🔎 Общее время выполнения линейного поиска: %.6f мс.\n", results.linear_time / CLOCKS_PER_MILLISEC);
    printf("🔀 Время сортировки Шелла: %.6f мс.\n", results.sort_time / CLOCKS_PER_MILLISEC);
    printf("🟨 Общее время выполнения поиска золотым сечением: %.6f мс.\n", results.gold_time / CLOCKS_PER_MILLISEC);
  
    // Освобождение памяти
    purgeArray(arr, ARRAY_LENGTH);
  }

  return 0;
}