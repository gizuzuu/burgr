#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "../lib/string.h"
#include "../lib/math.h"

const int ARRAY_LENGTH = 10000;     // Это N
const int ARRAY_STRING_LENGTH = 20; // Это char[20]

const double GOLDEN_RATIO = 1.61803398875;                // Золотое сечение
const double CLOCKS_PER_MILLISEC = CLOCKS_PER_SEC / 1000; // Для вывода времени в миллисекундах, а не секундах

/*
  Генерация (заполнение) псевдослучайной строки str из size символов
*/
void generateRandomString(char *str, int size)
{
  char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

  for (int i = 0; i < size - 1; i++)
  {
    // Выборка случайного индекса из charset
    int key = rand() % (int)(sizeof(charset) - 1);
    // Заполнение строки str случайным символом
    str[i] = charset[key];
  }

  // Установка метки конца строки
  str[size - 1] = '\0';
}

/*
  Заполнение массива строк arr длинной size случайными строками
*/
void fillArrayWithStrings(char **arr, int size)
{
  for (int i = 0; i < size; i++)
  {
    // Выделение памяти для строки
    arr[i] = (char *)malloc(ARRAY_STRING_LENGTH);
    // Генерация случайной строки
    generateRandomString(arr[i], ARRAY_STRING_LENGTH);
  }
}

/*
  Заполнение массива arr случайными выборками из source.
  size_arr - длинна массива arr, size_source - длинна масива source

  use_fake_string - 1, если надо генерировать элементы, не состоящие в массиве
    0, если надо генерировать элементы, состоящие в массиве
*/
void fillArrayWithRandomSelectedStrings(char **arr, char **source, int size_arr, int size_source, int use_fake_string)
{
  for (int i = 0; i < size_arr; i++)
  {
    // Выделение памяти для строки
    arr[i] = (char *)malloc(ARRAY_STRING_LENGTH);

    if (use_fake_string)
      strcpy_a(arr[i], "123af2253d\0"); // Вставка нереальной строки
    else
      strcpy_a(arr[i], source[rand() % size_source]); // Вставка одной из строк массива source
  }
}

/*
  Очистка массива строк
*/
void purgeArray(char **arr, int size)
{
  for (int i = 0; i < size; i++)
  {
    free(arr[i]);
  }

  free(arr);
}

/*
  Линейный поиск. Идём до конца массива с начала и ищем совпадения, после чего возвращаем
  индекс совпадения или -1, если ничего не было найдено
*/
int linearSearch(char **arr, char *target, int size)
{
  for (int i = 0; i < size; i++)
  {
    if (strcmp_a(arr[i], target) == 0)
      return i;
  }

  return -1;
}

int goldenSectionSearch(char *arr[], int size, char *target)
{
  int low = 0;
  int high = size - 1;
  int mid = high / GOLDEN_RATIO;

  while (strcmp_a(target, arr[mid]) != 0 && low < high && mid != 1)
  {
    int res = strcmp_a(target, arr[mid]);

    if (res < 0)
    {
      high = mid - 1;
    }
    else
    {
      low = mid + 1;
    }

    if (high - low == 1)
      mid = low + 1;
    else
      mid = low + (high - low) / GOLDEN_RATIO;
  }

  if (strcmp_a(arr[mid], target) == 0)
    return mid;

  return -1;
}

/*
  Сортировка Шелла.
  Принимает массив строк arr длинной size

  В начале функции вычисляется максимальное значение gap,
  которое будет использоваться в алгоритме. Это делается с помощью цикла for,
  который увеличивает gap до тех пор, пока оно не станет больше size.
  Значение gap вычисляется как 2^p - 1, где p - это текущая итерация цикла.

  Затем алгоритм выполняет серию “прогонов” через массив,
  начиная с максимального значения gap и уменьшая его на каждой итерации.
  На каждом прогоне элементы, разделенные текущим значением gap, сравниваются и,
  при необходимости, меняются местами.

  Внутренний цикл for проходит через массив, начиная с текущего значения gap и до конца массива.
  На каждой итерации выбирается элемент arr[i],
  и затем выполняется серия сравнений и обменов с элементами,
  находящимися на расстоянии gap слева от него.

  Если элемент arr[j - gap] больше текущего элемента temp, они меняются местами.
  Это продолжается, пока не будет найден элемент, который меньше temp,
  или пока не будет достигнут начало массива.
*/
void shellSort(char **arr, int size)
{
  // Вычисление максимального значения p, чтобы идти с конца, а не начала
  int max_p = 1;
  for (int p = 1, gap = 1; gap < size; gap = pow_a(2, p) - 1, p++)
    max_p = p;
  max_p -= 1;

  for (int p = max_p, gap = pow_a(2, max_p) - 1; p >= 0; gap = pow_a(2, p) - 1, p--)
  {
    for (int i = gap; i < size; i++)
    {
      char *temp = arr[i];
      int j;
      for (j = i; j >= gap && strcmp_a(arr[j - gap], temp) > 0; j -= gap) // сравнивает две строки лексикографически.
        arr[j] = arr[j - gap];
      arr[j] = temp;
    }
  }
}


int main()
{
  // Выделение памяти для массива строк
  char **arr = calloc(ARRAY_LENGTH, sizeof(char *));

  // Ввод параметров
  int m;
  printf("Введите m: ");
  scanf("%d", &m);

  if (m > ARRAY_LENGTH || m < 0)
  {
    printf("Число m должно быть от 0 до %d! Завершение программы\n", ARRAY_LENGTH);
    exit(0);
  }

  int use_fake_string;
  printf("Использовать несуществующие строки (1 - да, 0 - нет)? ");
  scanf("%d", &use_fake_string);

  char **m_arr = calloc(m, sizeof(char *));
  fillArrayWithStrings(arr, ARRAY_LENGTH);
  fillArrayWithRandomSelectedStrings(m_arr, arr, m, ARRAY_LENGTH, use_fake_string);

  double lin_start = clock();
  for (int i = 0; i < m; i++) {
    linearSearch(arr, m_arr[i], ARRAY_LENGTH);
  }
  double lin_time = clock() - lin_start;

  printf("🔎 Общее время выполнения линейного поиска: %.6f мс.\n", lin_time / CLOCKS_PER_MILLISEC);

  purgeArray(m_arr, m);
  m_arr = calloc(m, sizeof(char *));
  fillArrayWithRandomSelectedStrings(m_arr, arr, m, ARRAY_LENGTH, use_fake_string);

  double gold_start = clock();
  shellSort(arr, ARRAY_LENGTH);
  for (int i = 0; i < m; i++) {
    goldenSectionSearch(arr, ARRAY_LENGTH, m_arr[i]);
  }
  double gold_time = clock() - gold_start;

  printf("🟨 Общее время выполнения поиска золотым сечением с сортировкой Шелла: %.6f мс.\n", gold_time / CLOCKS_PER_MILLISEC);
  printf("📟 Общее время выполнения: %.6f мс.\n", (lin_time + gold_time) / CLOCKS_PER_MILLISEC);

  purgeArray(m_arr, m);
  purgeArray(arr, ARRAY_LENGTH);

  return 0;
}